package edu.luc.online;

import javax.servlet.http.HttpServletRequest;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.ui.Model;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import com.fasterxml.jackson.core.JsonProcessingException;
import com.fasterxml.jackson.databind.JsonMappingException;

import edu.luc.webservices.bean.ProductBean;

@RestController
public class AjaxController {
	
	@Autowired
	ProductService productService;
	
	@GetMapping(value="productdetail")
	public ProductBean getProductDetail(Model model, HttpServletRequest request) throws JsonMappingException, JsonProcessingException, ClassNotFoundException
	{
		String url = (String)request.getParameter("url");
	
		 return productService.getProductDetail(url);
		
	}
	
	

}
