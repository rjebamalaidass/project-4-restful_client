package edu.luc.webservices.bean;

import com.fasterxml.jackson.annotation.JsonProperty;

public class ProductOrder extends AbstractRepresentation {
	
	@JsonProperty("productId")
	public int productId;
	@JsonProperty("quantity")
	public int quantity;
	
	@JsonProperty("price")
	public double price;
	
	public double getPrice() {
		return price;
	}
	public void setPrice(double price) {
		this.price = price;
	}
	public int getProductId() {
		return productId;
	}
	public void setProductId(int productId) {
		this.productId = productId;
	}
	public int getQuantity() {
		return quantity;
	}
	public void setQuantity(int quantity) {
		this.quantity = quantity;
	}
	@Override
	public String toString() {
		return "ProductOrder [productId=" + productId + ", quantity=" + quantity + ", price=" + price + "]";
	}
	
	

}
