package edu.luc.webservices.bean;

import java.util.HashMap;

import javax.persistence.Column;
import javax.xml.bind.annotation.XmlAccessType;
import javax.xml.bind.annotation.XmlAccessorType;
import javax.xml.bind.annotation.XmlRootElement;
import javax.xml.bind.annotation.XmlType;

import org.springframework.context.annotation.Configuration;

import com.fasterxml.jackson.annotation.JsonProperty;

//@XmlRootElement(name = "Product")
//@XmlAccessorType(XmlAccessType.FIELD)
//@XmlType(name = "")
@Configuration
public class ProductBean  extends AbstractRepresentation {
	
	@JsonProperty("productid")
	private int productid;
	
	  @JsonProperty("productname")
	private String productname;
	
	  @JsonProperty("description")
	private String description;
	
	  @JsonProperty("category")
	private String category;
	
	  @JsonProperty("price")
	private double price;

	
	  @JsonProperty("availablequantity")
	private int availablequantity;
	  
	  private String detailurl;
	  
	  private String orderurl;
	  
	  
	

	public String getDetailurl() {
		return detailurl;
	}


	public void setDetailurl(String detailurl) {
		this.detailurl = detailurl;
	}


	public String getOrderurl() {
		return orderurl;
	}


	public void setOrderurl(String orderurl) {
		this.orderurl = orderurl;
	}


	public int getProductid() {
		return productid;
	}


	public void setProductid(int productid) {
		this.productid = productid;
	}


	public String getProductname() {
		return productname;
	}


	public void setProductname(String productname) {
		this.productname = productname;
	}


	public String getDescription() {
		return description;
	}


	public void setDescription(String description) {
		this.description = description;
	}


	public String getCategory() {
		return category;
	}


	public void setCategory(String category) {
		this.category = category;
	}


	public double getPrice() {
		return price;
	}


	public void setPrice(double price) {
		this.price = price;
	}




	public int getAvailablequantity() {
		return availablequantity;
	}


	public void setAvailablequantity(int availablequantity) {
		this.availablequantity = availablequantity;
	}


	@Override
	public String toString() {
		return "ProductBean [productid=" + productid + ", productname=" + productname + ", description=" + description
				+ ", category=" + category + ", price=" + price + ", availablequantity=" + availablequantity
				+ ", detailurl=" + detailurl + ", orderurl=" + orderurl + "]";
	}





	
	

}
