package edu.luc.webservices.bean;


import java.util.List;

public class AbstractRepresentation {
protected List<Link> links;
	
	public List<Link> getLinks() {
		return links;
	}
	
	public void setLinks(List<Link> linkList) {
		this.links = linkList;
	}

}
