package edu.luc.webservices.bean;

public class Order {
	
	private String orderid;
	private String name;
	public String getName() {
		return name;
	}
	public void setName(String name) {
		this.name = name;
	}
	private String productid;
	private String  quantity;
	private String price;
	private String addressid;
	private String addresstype;
	private String address1;
	private String address2;
	private String city;
	private String state;
	private String zip;
	private String orderstatus;
	private String orderprice;
	private String shippingcost;
	private String totalprice;
	private String orderdate;
	public String getOrderdate() {
		return orderdate;
	}
	public void setOrderdate(String orderdate) {
		this.orderdate = orderdate;
	}
	private String orderdetailurl;
	private String paymenturl;
	private String producturl;
	private String cancelurl;
	private String updateurl;
	private String shippingurl;
	public String getShippingurl() {
		return shippingurl;
	}
	public void setShippingurl(String shippingurl) {
		this.shippingurl = shippingurl;
	}
	public String getOrderid() {
		return orderid;
	}
	public void setOrderid(String orderid) {
		this.orderid = orderid;
	}
	public String getProductid() {
		return productid;
	}
	public void setProductid(String productid) {
		this.productid = productid;
	}
	public String getQuantity() {
		return quantity;
	}
	public void setQuantity(String quantity) {
		this.quantity = quantity;
	}
	public String getPrice() {
		return price;
	}
	public void setPrice(String price) {
		this.price = price;
	}
	public String getAddressid() {
		return addressid;
	}
	public void setAddressid(String addressid) {
		this.addressid = addressid;
	}
	public String getAddresstype() {
		return addresstype;
	}
	public void setAddresstype(String addresstype) {
		this.addresstype = addresstype;
	}
	public String getAddress1() {
		return address1;
	}
	public void setAddress1(String address1) {
		this.address1 = address1;
	}
	public String getAddress2() {
		return address2;
	}
	public void setAddress2(String address2) {
		this.address2 = address2;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getState() {
		return state;
	}
	public void setState(String state) {
		this.state = state;
	}
	public String getZip() {
		return zip;
	}
	public void setZip(String zip) {
		this.zip = zip;
	}
	public String getOrderstatus() {
		return orderstatus;
	}
	public void setOrderstatus(String orderstatus) {
		this.orderstatus = orderstatus;
	}
	public String getOrderprice() {
		return orderprice;
	}
	public void setOrderprice(String orderprice) {
		this.orderprice = orderprice;
	}
	public String getShippingcost() {
		return shippingcost;
	}
	public void setShippingcost(String shippingcost) {
		this.shippingcost = shippingcost;
	}
	public String getTotalprice() {
		return totalprice;
	}
	public void setTotalprice(String totalprice) {
		this.totalprice = totalprice;
	}
	public String getOrderdetailurl() {
		return orderdetailurl;
	}
	public void setOrderdetailurl(String orderdetailurl) {
		this.orderdetailurl = orderdetailurl;
	}
	public String getPaymenturl() {
		return paymenturl;
	}
	public void setPaymenturl(String paymenturl) {
		this.paymenturl = paymenturl;
	}
	public String getProducturl() {
		return producturl;
	}
	public void setProducturl(String producturl) {
		this.producturl = producturl;
	}
	public String getCancelurl() {
		return cancelurl;
	}
	public void setCancelurl(String cancelurl) {
		this.cancelurl = cancelurl;
	}
	public String getUpdateurl() {
		return updateurl;
	}
	public void setUpdateurl(String updateurl) {
		this.updateurl = updateurl;
	}
	@Override
	public String toString() {
		return "Order [orderid=" + orderid + ", name=" + name + ", productid=" + productid + ", quantity=" + quantity
				+ ", price=" + price + ", addressid=" + addressid + ", addresstype=" + addresstype + ", address1="
				+ address1 + ", address2=" + address2 + ", city=" + city + ", state=" + state + ", zip=" + zip
				+ ", orderstatus=" + orderstatus + ", orderprice=" + orderprice + ", shippingcost=" + shippingcost
				+ ", totalprice=" + totalprice + ", orderdate=" + orderdate + ", orderdetailurl=" + orderdetailurl
				+ ", paymenturl=" + paymenturl + ", producturl=" + producturl + ", cancelurl=" + cancelurl
				+ ", updateurl=" + updateurl + ", shippingurl=" + shippingurl + "]";
	}
	
	
	

}
